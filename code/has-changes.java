boolean isTestAffectedByChanges(TestMethodCall test,
		Clover2Registry registry,
		Set<String> changedFiles) {
  final Map<String, SourceState> perTestStates =
	  perTestSourceStates.get(test);
  final boolean isAffected = perTestStates == null ||
	  hasChanges(test, perTestStates.keySet(), changedFiles);
  [...]
  return isAffected;
}
private boolean hasChanges(TestMethodCall testMethod,
		Set<String> perTestFiles, 
		Set<String> changedFiles) {
  return !Collections.disjoint(perTestFiles, changedFiles);
}
