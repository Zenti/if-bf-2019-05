public Set<TestMethodCall> getTestsAffectedByDiff(Set<String> fileNames) {
  Set<TestMethodCall> result = new HashSet<TestMethodCall>();
  Map<String, Collection<TestMethodCall>> f2TM = getFile2TestsMap();
  for (String fileName : fileNames) {
    Collection<TestMethodCall> tmc = f2TM.get(fileName);
    if(tmc != null) {
      result.addAll(tmc);
    }
  }
  return result;
}
