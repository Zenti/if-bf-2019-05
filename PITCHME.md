---?image=traefik.png&position=50% 15%&size=auto 60%

## Was bisher bei IF-Infrastruktur geschah...

---?image=images/old-if-lab-infra.png&size=auto 100%

Note:

Einer der Folien nach dem ersten Wurf

+++?image=images/if-lab-infra.png&size=auto 100%

Note:

Präsentation auf unternehmensweiten Blue Friday

+++?image=images/updated-infra.png&size=auto 100%

Note:

Bisheriger Stand

---

## Und nun, die Fortsetzung

---

![Jenkins](images/jenkins.svg)

Note:

Vorher: dicke Jenkins-Maschine, die alles macht.
Die auch mal abgestürzt ist, wenn zu viel Last auf der Maschine war.
Extra-Maschine für große UI-Tests.

+++

![Jenkins on Fire](images/jenkins-fire.svg)

+++

![ItsDead](images/itsdead.jpg)

+++

![GitLab Runner](images/gitlab_runner.png)

+++

![Docker Machine Logo](images/docker-machine-logo.png)

+++

![Docker Machine](images/docker-machine.png)

Note:

Nachher: Ein kleiner Koordinator, der bei Bedarf neue Maschinen hochfährt.
Nutzt GitLab CI Runner mit Docker Machine als Framework

+++

### Templates

- Standard, t3.medium, von 7-19 Uhr Mo-Fr 2x idle
- HighRAM, t3.large, nur bei Bedarf (Tag "highram")
- Selenium, t3.large, nur bei Bedarf (Tag "selenium"), Shared Memory mounted

---

## Weitere Änderungen

+++

![Quality?](images/quality.gif)

+++

![SonarQube](images/sonarqube.png)

+++

![Graylog](images/graylog.svg)

+++

Registry und Runner Cache auf S3 - docker pull verweist auf

### if-lab-container-registry.s3.eu-central-1.amazonaws.com

---

![Kubernetes](images/kubernetes.svg)

Note:

Plan: Kubernetes ausrollen, soll Integration und Staging ersetzen

+++

![Noch im Test](images/this-is-fine.jpg)

